/*
////////////////////////
    Selecting Elements
////////////////////////
*/

let formSection = document.querySelector("#tourist_form");
let gridSelection = document.querySelector("#tourist_grid");
let name = document.querySelector("#name");
let address = document.querySelector("#address");
let rating = document.querySelector("#rating");
let picture = document.querySelector("#picture");
let preview = document.querySelector("#imagePreview");



let form = document.querySelector("#form");
let reset_btn = document.querySelector("#reset_btn");
let submit_btn = document.querySelector("#submit_btn");
let back_btn = document.querySelector("#back_btn");
let create_btn = document.querySelector("#create_btn");
var update_btn;
var delete_btn;
let action_container = document.querySelector(".action_container");


let globalIndex = 0;
let table_ctn = document.querySelector("#table");
let search = document.querySelector("#search");
var imgSrc = "";
var flag = -1;

//let places = [];


/*
////////////////////////
    Button Listener
////////////////////////
*/


function previewFile(){
    const preview = document.querySelector("#imagePreview");
    const file = document.querySelector("input[type=file]").files[0];
    const reader = new FileReader();
    let acceptable = file.name.endsWith(".jpg") || file.name.endsWith(".jpeg") || file.name.endsWith(".png");

    if(!acceptable){
        window.alert("Please upload a valid image file");
        document.querySelector("input[type=file]").value = null;
        return;
    }

    reader.addEventListener("load", function(){
        preview.src = reader.result;
        imgSrc = reader.result;
    }, false);

    if(file){
        reader.readAsDataURL(file);
    }
}

reset_btn.addEventListener("click",
    function (e)
    {
        name.value = "";
        address.value = "";
        rating.value = "";
        picture.value = "";
        preview.src = "img/icon.png";
    }
);

form.addEventListener("submit",
    function(e){
        e.preventDefault();

        submit();
    }
);

back_btn.addEventListener("click",
    function (e)
    {
        e.preventDefault();

        document.getElementById("tourist_form").style.display = "none";
        document.getElementById("tourist_grid").style.display = "block";

        display();
    }
);

create_btn.addEventListener("click",
    function (e)
    {
        e.preventDefault();

        name.value = "";
        address.value = "";
        rating.value = "";
        picture.value = "";
        preview.src = "img/icon.png";

        document.getElementById("tourist_form").style.display = "block";
        document.getElementById("tourist_grid").style.display = "none";
    }
);



search.addEventListener("input",
    function(e){
        let places = JSON.parse(localStorage.getItem("places"));
        const searchVal = e.target.value;

        let characters = "";

        for(let i = 0; i<searchVal.length; i++){
            if(searchVal[i] != " "){
                characters += ".*" + searchVal[i];
            }
        }

        if(characters.length != 0){
            characters += ".*";
        }

        let matches = places.filter(
            x => {
                const regEx = new RegExp(`${characters}`, "gi");
                return x.name.match(regEx);
            }
        )

        searchDisplay(matches);
    }
);


function submit(){
    if(flag>-1){

        let places = JSON.parse(localStorage.getItem("places"));
        let length = places.length;

        for(let i=0;i<length;i++){
            let place = places[i];

            if(place.id == flag){
                place.name = name.value;
                place.address = address.value;
                place.rating = rating.value;
                place.picture = preview.src;
                
                break;
            }
        }
        localStorage.setItem("places", JSON.stringify(places));
        flag = -1;
        display();

    }
    else{
        let index = localStorage.getItem("index");
        if(index == null) {
            index = globalIndex; 
            localStorage.setItem("index", index);
        }
        index = parseInt(index);
        let nameValue = name.value;
        let addressValue = address.value;
        let ratingValue = rating.value;
        let pictureValue = imgSrc;


        let place = new Object();
        place.id = index;
        place.name = nameValue;
        place.address = addressValue;
        place.rating = ratingValue;
        place.picture = pictureValue;
        
        let places;
        console.log(localStorage.getItem("places"));
        if(localStorage.getItem("places") == null){
            places = [];
            places.push(place);
            localStorage.setItem("places", JSON.stringify(places));
        }
        else{
            console.log(localStorage["places"]);
            places = JSON.parse(localStorage.getItem("places"));
            places.push(place);
            console.log(nameValue);
        }

        localStorage.setItem("places", JSON.stringify(places));

        index = index + 1;
        localStorage.setItem("index", index);

        display();
    }
    
};




function display(){
    let grid_items = gridSelection.querySelectorAll('tr');
    let tr_length = grid_items.length;
    for(let i=tr_length-1; i>0; i--){
        grid_items[i].remove();
    }

    console.log('display');
    let places = JSON.parse(localStorage.getItem("places"));
    let length = places.length;
    console.log(length);
    places = JSON.parse(localStorage.getItem("places"));

    for(let i=0;i<length;i++){
        let place = places[i];

        let uid = "u" + place.id;
        let did = "d" + place.id;

        let row = document.createElement('tr');
        let nameData = document.createElement('td');
        let addressData = document.createElement('td');
        let ratingData = document.createElement('td');
        let pictureData = document.createElement('td');
        let image = document.createElement('img');
        let actionData = document.createElement('td');
        let actionContainerDiv = document.createElement('div');
        let deleteLink = document.createElement('a');
        let updateLink = document.createElement('a');

        nameData.innerHTML = place.name;
        addressData.innerHTML = place.address;
        ratingData.innerHTML = place.rating;
        //pictureData.innerHTML = place.picture;
        image.setAttribute("src", place.picture);
        image.setAttribute("width", 250);
        image.setAttribute("height", 200);
        actionContainerDiv.setAttribute("class", "action_container");
        deleteLink.setAttribute("href", "#");
        updateLink.setAttribute("href", "#");
        deleteLink.setAttribute("id", did);
        updateLink.setAttribute("id", uid);
        deleteLink.setAttribute("class", "delete_btn");
        updateLink.setAttribute("class", "update_btn");
        let deleteLinkText = document.createTextNode("Delete");
        let updateLinkText = document.createTextNode("Update");
        deleteLink.appendChild(deleteLinkText);
        updateLink.appendChild(updateLinkText);


        row.appendChild(nameData);
        row.appendChild(addressData);
        row.appendChild(ratingData);
        row.appendChild(pictureData);
        row.appendChild(actionData);
        pictureData.appendChild(image);
        actionContainerDiv.appendChild(deleteLink);
        actionContainerDiv.appendChild(updateLink);
        actionData.appendChild(actionContainerDiv);

        table_ctn.appendChild(row);

        document.getElementById("tourist_form").style.display = "none";
        document.getElementById("tourist_grid").style.display = "block";


        update_btn = document.querySelector(`#${uid}`);
        delete_btn = document.querySelector(`#${did}`);

        delete_btn.addEventListener("click",
            function(e){
                console.log("Delete");
                let cid = e.target.id;
                let idx = cid.slice(1);
                
                let places = JSON.parse(localStorage.getItem("places"));
                let length = places.length;
                let newplaces = [];
            
                for(let i=0;i<length;i++){
                    let place = places[i];
            
                    if(place.id == idx){ continue;}
                    else{
                        newplaces.push(place);
                    }
                }

                places = newplaces;
                localStorage.setItem("places", JSON.stringify(places));
                display();
            }
            
        );

        update_btn.addEventListener("click",
            function(e){
                let cid = e.target.id;
                let idx = cid.slice(1);
            
                let places = JSON.parse(localStorage.getItem("places"));
                let length = places.length;
            
                for(let i=0;i<length;i++){
                    let place = places[i];
            
                    if(place.id == idx){
                        name.value = place.name;
                        address.value = place.address;
                        rating.value = place.rating;
                        preview.src = place.picture;
                        
                        document.getElementById("tourist_form").style.display = "block";
                        document.getElementById("tourist_grid").style.display = "none";
            
                        flag = idx;
                        break;
                    }
                }
            }   
        );

    }

        
};

function searchDisplay(matchedPlaces){
    let grid_items = gridSelection.querySelectorAll('tr');
    let tr_length = grid_items.length;
    for(let i=tr_length-1; i>0; i--){
        grid_items[i].remove();
    }
    
    let length = matchedPlaces.length;
    console.log(length);
    places = JSON.parse(localStorage.getItem("places"));

    for(let i=0;i<length;i++){
        let place = matchedPlaces[i];

        let uid = "u" + place.id;
        let did = "d" + place.id;

        let row = document.createElement('tr');
        let nameData = document.createElement('td');
        let addressData = document.createElement('td');
        let ratingData = document.createElement('td');
        let pictureData = document.createElement('td');
        let image = document.createElement('img');
        let actionData = document.createElement('td');
        let actionContainerDiv = document.createElement('div');
        let deleteLink = document.createElement('a');
        let updateLink = document.createElement('a');

        nameData.innerHTML = place.name;
        addressData.innerHTML = place.address;
        ratingData.innerHTML = place.rating;
        //pictureData.innerHTML = place.picture; 
        image.setAttribute("src", place.picture);
        image.setAttribute("width", 250);
        image.setAttribute("height", 200);
        actionContainerDiv.setAttribute("class", "action_container");
        deleteLink.setAttribute("href", "#");
        updateLink.setAttribute("href", "#");
        deleteLink.setAttribute("id", did);
        updateLink.setAttribute("id", uid);
        deleteLink.setAttribute("class", "delete_btn");
        updateLink.setAttribute("class", "update_btn");
        let deleteLinkText = document.createTextNode("Delete");
        let updateLinkText = document.createTextNode("Update");
        deleteLink.appendChild(deleteLinkText);
        updateLink.appendChild(updateLinkText);


        row.appendChild(nameData);
        row.appendChild(addressData);
        row.appendChild(ratingData);
        row.appendChild(pictureData);
        row.appendChild(actionData);
        pictureData.appendChild(image);
        actionContainerDiv.appendChild(deleteLink);
        actionContainerDiv.appendChild(updateLink);
        actionData.appendChild(actionContainerDiv);

        table_ctn.appendChild(row);

        document.getElementById("tourist_form").style.display = "none";
        document.getElementById("tourist_grid").style.display = "block";

        update_btn = document.querySelector(`#${uid}`);
        delete_btn = document.querySelector(`#${did}`);

        delete_btn.addEventListener("click",
            function(e){
                console.log("Delete");
                let cid = e.target.id;
                let idx = cid.slice(1);
            
                let places = JSON.parse(localStorage.getItem("places"));
                let length = places.length;
                let newplaces = [];
            
                for(let i=0;i<length;i++){
                    let place = places[i];
            
                    if(place.id == idx){ continue;}
                    else{
                        newplaces.push(place);
                    }
                }

                places = newplaces;
                localStorage.setItem("places", JSON.stringify(places));
                display();
            }
            
        );

        update_btn.addEventListener("click",
            function(e){
                let cid = e.target.id;
                let idx = cid.slice(1);
        
                let places = JSON.parse(localStorage.getItem("places"));
                let length = places.length;
            
                for(let i=0;i<length;i++){
                    let place = places[i];
            
                    if(place.id == idx){
                        name.value = place.name;
                        address.value = place.address;
                        rating.value = place.rating;
                        preview.src = place.picture;
                        
                        document.getElementById("tourist_form").style.display = "block";
                        document.getElementById("tourist_grid").style.display = "none";
            
                        flag = idx;
                        break;
                    }
                }
            }   
        );

    }

    
    
};




/*
//////////////////////////////////
    Sorting Function
//////////////////////////////////
*/

function sortTable(){
    console.log("table");
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("table");
    switching = true;
    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.rows;

        for (i = 1; i < (rows.length - 1); i++) {

            shouldSwitch = false;

            x = rows[i].getElementsByTagName("TD")[2];
            y = rows[i + 1].getElementsByTagName("TD")[2];

            console.log(dir);
            if (dir == "asc") {
                console.log(x);
                console.log(y);

                if (x.innerHTML > y.innerHTML) {
                    console.log(x.innerHTML);
                    console.log(y.innerHTML);
                    shouldSwitch = true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML < y.innerHTML) {
                    console.log(x.innerHTML);
                    console.log(y.innerHTML);
                    shouldSwitch = true;
                    break;
                }
            }
        }
        console.log(shouldSwitch);
        if (shouldSwitch) {

            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;

            switchcount ++;
        } else {

            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
};